package arrays;
/*
Задание 5.
Посчитать сумму элементов массива с нечетными индексами
 */
public class Fifth {
    public static void main(String[] args){
        int nums[] = new int[]{1, 4, -2, 3};
        int sum = 0;
        for (int i = 0; i<nums.length;i++) {
            if (i%2!=0) {
                sum+=nums[i];
            }
        }
        System.out.println(sum);
    }
}

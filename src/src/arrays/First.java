package arrays;
/*
Задание 1.
Найти минимальный элемент массива
 */
public class First {
    public int minValueOfArray(int[] nums){
        int min = nums[0];
        for (int num : nums) {
            if (num < min) {
                min = num;
            }
        }
        return min;
    }
}

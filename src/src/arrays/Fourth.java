package arrays;
/*
Задание 4.
Найти индекс максимального элемента массива
 */
public class Fourth {
    public static void main(String[] args){
        int nums[] = new int[]{0, 25, 5, -6};
        int max = nums[0];
        int index = 0;
        for (int i = 0; i<nums.length;i++) {
            if (nums[i]> max) {
                max = nums[i];
                index = i;
            }
        }
        System.out.println(index);
    }
}

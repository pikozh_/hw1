package arrays;
/*
Задание 7.
Посчитать количество нечетных элементов массива
 */
public class Sevenht {
    public int quantityOfOddElements(int[] nums){
        int count = 0;
        for (int i = 0; i<nums.length;i++) {
            if (nums[i]%2!=0) {
                count++;
            }
        }
        return count;
    }
}

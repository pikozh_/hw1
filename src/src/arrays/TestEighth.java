package arrays;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class TestEighth {
    @Test
    public void testFirst() {
        Eighth test = new Eighth();
        int[] testArray = new int[]{1, 3, 5, 4, 7};
        int[] testValue = test.reverseHalfOfArray(testArray);
        int[] expectedArray = {4,7,5,1,3};
        Assert.assertEquals(Arrays.toString(expectedArray), Arrays.toString(testValue));
    }
}

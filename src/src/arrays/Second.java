package arrays;
/*
Задание 2.
Найти максимальный элемент массива
 */
public class Second {
    public int maxValueOfArray(int[] nums) {

        int max = nums[0];
        for(int num : nums){
            if (num > max) {
                max = num;
            }
        }
        return max;
}
}


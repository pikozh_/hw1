package arrays;
/*
Задание 3.
Найти индекс минимального элемента массива
 */
public class Third {
    public int minIndexOfArray(int[] nums){
        int min = nums[0];
        int index = 0;
        for (int i = 0; i<nums.length;i++) {
            if (nums[i]< min) {
                min = nums[i];
                index = i;
            }
        }
        return index;
    }
}

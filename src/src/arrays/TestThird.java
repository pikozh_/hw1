package arrays;

import junit.framework.Assert;
import org.junit.Test;

public class TestThird {
    @Test
    public void testThird() {
        Third test = new Third();
        int[] testArray = new int[]{1, 3, 5, 4};
        int testValue = test.minIndexOfArray(testArray);
        Assert.assertEquals(0, testValue);
    }
}

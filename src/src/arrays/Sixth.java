package arrays;
/*
Задание 6.
Сделать реверс массива (массив в обратном направлении)
 */
import java.util.Arrays;

public class Sixth {
    public static void main(String[] args) {
        int nums[] = new int[]{1, 4, 2, 3};
        for (int i = 0; i < nums.length/2; i++) {
            int tmp = nums[i];
            nums[i] = nums[nums.length - i - 1];
            nums[nums.length - i - 1] = tmp;
        }
        System.out.println(Arrays.toString(nums));
    }
}

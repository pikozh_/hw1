package arrays;

import org.junit.Assert;
import org.junit.Test;

public class TestFirst {
    @Test
    public void testFirst() {
        First test = new First();
        int[] testArray = new int[]{1, 3, 5, 3};
        int testValue = test.minValueOfArray(testArray);
        Assert.assertEquals(1, testValue);
    }
}

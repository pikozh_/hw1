package arrays;
/*
Задание 9.
Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))
 */
import java.util.Arrays;

public class Ninth {
    public static void main(String[] args){

// Bubble sort
        int[] nums1 = new int[]{6,2,9,5,1,4,3};
        for(int i = nums1.length-1 ; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++){

            if( nums1[j] > nums1[j+1] ){
                int tmp = nums1[j];
                nums1[j] = nums1[j+1];
                nums1[j+1] = tmp;
            }
        }
    }
        System.out.println(Arrays.toString(nums1));


// Select sort
        int[] nums2 = new int[]{6,2,9,5,1,4,3};
        for (int i = 0; i < nums2.length; i++) {
            int min = nums2[i];
            int min_i = i;
            for (int j = i+1; j < nums2.length; j++) {
                if (nums2[j] < min) {
                    min = nums2[j];
                    min_i = j;
                }
            }
            if (i != min_i) {
                int tmp = nums2[i];
                nums2[i] = nums2[min_i];
                nums2[min_i] = tmp;
            }
        }
        System.out.println(Arrays.toString(nums2));


//Insert sort
        int[] nums3 = new int[]{6,2,9,5,1,4,3};
        for (int left = 0; left < nums3.length; left++) {
            int value = nums3[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < nums3[i]) {
                    nums3[i + 1] = nums3[i];
                } else {

                    break;
                }
            }
            nums3[i + 1] = value;
        }
        System.out.println(Arrays.toString(nums3));
    }

}

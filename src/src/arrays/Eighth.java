package arrays;
/*
Задание 8.
Поменять местами первую и вторую половину массива, например, для массива 1 2 3
4, результат 3 4 1 2
 */
import java.util.Arrays;

public class Eighth {
    public int[] reverseHalfOfArray(int[] nums) {
        for (int i = 0; i < nums.length/2; i++) {
            int tmp = nums[i];

// To avoid ArrayIndexOutOfBoundsException we use different variations for even and odd nums.length

            if(nums.length%2==0){
            nums[i] = nums[nums.length/2 + i];
            nums[nums.length/2 + i] = tmp;
            }
            else{
                nums[i] = nums[nums.length/2 + i+1];
                nums[nums.length/2 + i+1] = tmp;
            }
        }
        return nums;
    }
}

package arrays;

import junit.framework.Assert;
import org.junit.Test;

public class TestSecond {
    @Test
    public void testSecond() {
        Second test = new Second();
        int[] testArray = new int[]{1, 3, 5, 4};
        int testValue = test.maxValueOfArray(testArray);
        Assert.assertEquals(5, testValue);
    }
}

package operators;

import org.junit.Assert;
import org.junit.Test;

public class TestFirst {
    @Test
    public void testSumOrMultiply() {
        First test = new First();
        int testValue = test.sumOrMultiply(2,7);
        Assert.assertEquals(14,testValue);
    }
}

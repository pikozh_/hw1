package operators;
/*
Задание 5.
Написать программу определения оценки студента по его рейтингу
 */
import java.util.Scanner;

public class Fifth {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your point: ");
        int point = scanner.nextInt();

        if (point>=0 && point<=19){
            System.out.println("You have F");
        }

         else if (point>=20 && point<=39){
            System.out.println("You have E");
        }

         else if (point>=40 && point<=59){
            System.out.println("You have D");
        }

         else if (point>=60 && point<=74){
            System.out.println("You have C");
        }

         else if (point>=75 && point<=89){
            System.out.println("You have B");
        }

         else if (point>=90 && point<=100){
            System.out.println("You have A");
        }

    }
}

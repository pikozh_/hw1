package operators;
/*
Задание 3.
Найти суммы только положительных из трех чисел.
 */
import java.util.Scanner;

public class Third {

    public static void main(String[] args) {
        int sum = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a:");
        int a = scanner.nextInt();
        if (a > 0)
            sum = sum + a;

        System.out.print("Enter b:");
        int b = scanner.nextInt();
        if (b>0)
            sum = sum + b;

        System.out.print("Enter c:");
        int c = scanner.nextInt();
        if (c>0)
            sum = sum + c;

        System.out.println("Sum of all positive numbers: "+ sum);


    }
}
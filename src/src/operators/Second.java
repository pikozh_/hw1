package operators;
/*
Задание 2.
Определить какой четверти принадлежит точка с координатами (х,у)
 */
import java.util.Scanner;

public class Second {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter X-axis:");
        int x = scanner.nextInt();
        System.out.print("Enter Y-axis:");
        int y = scanner.nextInt();
        String quadrant = quadrantDeterminant(x,y);
        System.out.println("Dot with coordinates (" + x + "," + y + ") lies in "+quadrant+ " quadrant");
    }
    public static String quadrantDeterminant(int x, int y) {
        if (x > 0 && y > 0) {
            return "FIRST";
        } else if (x < 0 && y > 0) {
            return "SECOND";
        } else if (x < 0 && y < 0) {
            return "THIRD";
        } else if (x > 0 && y < 0) {
            return "FOURTH";
        } else
            return "COORDINATE AXIS";
    }
    }

package operators;
/*
Задание 4.
Посчитать выражение макс(а*б*с, а+б+с)+3
 */
import java.util.Scanner;

public class Fourth {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter A:");
        int a = scanner.nextInt();

        System.out.print("Enter B:");
        int b = scanner.nextInt();

        System.out.print("Enter C:");
        int c = scanner.nextInt();

        int k = Math.max(a*b*c, a+b+c)+3;

        System.out.println(k);
    }
}

package cycle;

import java.util.Scanner;

/*
Задание 3
Найти корень натурального числа с точностью до целого (рассмотреть вариант последовательного подбора и метод бинарного поиска)
 */

public class Third {

            // Метод последовательного подбора
    public int sqrtSeq(int key)
    {
        for (int i=1; ; i++ )
        {
            int q = i * i;
            if (key == q)
                return i;
            if (key < q)
                return i-1;
        }
    }
             // Метод бинарного поиска
    public int sqrtBinary(int key)
    {
        int min = 1;
        int max = key;
        int prev = 0;
        for (;;)
        {
            int mid = (min + max) / 2;
            if (prev == mid)
                return mid;
            int q = mid * mid;
            if (key == q)
                return mid;
            if (key < q)
                max = mid;
            else
                min = mid;
            prev = mid;
        }
    }
}

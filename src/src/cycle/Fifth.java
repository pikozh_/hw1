package cycle;
/*
Задание 5
Посчитать сумму цифр заданного числа
 */
import java.util.Scanner;

public class Fifth {
    public int sumOfDigits(int num){
        int sum = 0;
        String number = Integer.toString(num);
        char[] a = number.toCharArray();
        for(int i = 0; i<a.length;i++){
            int k = Character.getNumericValue(a[i]);
            sum = sum + k;
        }
        return sum;
    }
}

package cycle;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TestSixth {
    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }

    @Test
    public void testString() {
        Sixth test = new Sixth();
        test.mirror(123);
        Assert.assertEquals("321", output.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }
}


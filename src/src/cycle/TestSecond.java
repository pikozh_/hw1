package cycle;

import org.junit.Assert;
import org.junit.Test;

public class TestSecond {

    @Test
    public void testSimpleNumberCheck() {
        Second test = new Second();
        boolean testValue = test.simpleNumberCheck(23);
        Assert.assertEquals(true, testValue);
    }
}

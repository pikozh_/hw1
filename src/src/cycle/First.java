package cycle;
/*
Задание 1
Найти сумму четных чисел и их количество в диапазоне от 1 до 99
 */
public class First {
    public int sumOfEvenElements1to99(){
        int sum = 0;
        for(int i = 1; i<100; i++){
            if (i%2==0){
                sum = sum+i;
            }
        }
        return sum;
    }
    public int quantityOfEvenElements1to99(){
        int count = 0;
        for(int i = 1; i<100; i++){
            if (i%2==0){
                count++;
            }
    }
        return count;
}
}

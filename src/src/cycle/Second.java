package cycle;
/*
Задание 2.
Проверить простое ли число? (число называется простым, если оно делится только само на себя и на 1)
 */

public class Second {
    public boolean simpleNumberCheck(int number){
        for ( int i=2; i < number; i++) {
            if ( number%i == 0) {
                return false;
            }
        }
        return true;
    }
}

package cycle;
/*
Задание 6
Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например, задано число 123, вывести 321.
 */
import java.util.Scanner;

public class Sixth {
    public void mirror(int numb){
        String number = Integer.toString(numb);
        char[] d = number.toCharArray();
        for(int i = d.length-1;i>=0;i--)
            System.out.print(d[i]);
    }
}

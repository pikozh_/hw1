package cycle;
/*
Задание 4
Вычислить факториал числа n. n! = 1*2*…*n-1*n;!
 */
import java.util.Scanner;

public class Fourth {
    public int factorial(int number){
        int factorial = 1;

        for(int i = 1; i<=number; i++){
            factorial = factorial*i;
        }
        return factorial;
    }
}

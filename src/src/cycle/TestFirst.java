package cycle;

import org.junit.Assert;
import org.junit.Test;

public class TestFirst {
    First test = new First();
    @Test
    public void testSumOfEvenElements1to99() {
        int testValue = test.sumOfEvenElements1to99();
        Assert.assertEquals(2450, testValue);
    }
    @Test
    public void testQuantityOfEvenElements1to99() {
        int testValue = test.quantityOfEvenElements1to99();
        Assert.assertEquals(49, testValue);
    }

}

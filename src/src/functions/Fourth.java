package functions;
/*
Задание 4.
Найти расстояние между двумя точками в двумерном декартовом пространстве.
 */
import java.util.Scanner;

public class Fourth {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter X1: ");
        double x1 = scanner.nextInt();
        System.out.print("Enter Y1: ");
        double y1 = scanner.nextInt();

        System.out.print("Enter X2: ");
        double x2 = scanner.nextInt();
        System.out.print("Enter Y2: ");
        double y2 = scanner.nextInt();

        System.out.println("The distance is "+ distance(x1,y1,x2,y2));
    }

    public static double distance(double x1, double y1, double x2, double y2 ){
        double ac = Math.abs(y2 - y1);
        double cb = Math.abs(x2 - x1);
        return Math.hypot(ac, cb);
    }
}

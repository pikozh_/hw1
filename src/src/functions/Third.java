package functions;
/*
Задание 3.
Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число
 */
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Third {


        private static final String[][] sampleText = {{"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять ",
                "одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать",
                "шестнадцать ", "семьнадцать", "восемьнадцать", "девятнадцать"},
                {"","","двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"},
                {"","сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"}};



        public static int numbersAdapter(String number) {

            Map<String, Integer> numberAdapter = new HashMap<String, Integer>();

            for(int i =0; i<=19; i++) {
                numberAdapter.put(sampleText[0][i],i);
            }
            for(int i =2; i<=9; i++) {
                numberAdapter.put(sampleText[1][i],i*10);
            }
            for(int i =1; i<=9; i++) {
                numberAdapter.put(sampleText[2][i],i*100);
            }

            String exit = "";
            String[] a = number.split(" ");


            if(a.length==1){
                exit = exit+numberAdapter.get(a[0]);
            }
            else if(a.length==2){
                exit = exit+(numberAdapter.get(a[0])+numberAdapter.get(a[1]));
            }
            else if(a.length==3){
                exit = exit+(numberAdapter.get(a[0])+numberAdapter.get(a[1])+numberAdapter.get(a[2]));
            }

            int exitt = Integer.parseInt(exit);
            return exitt;

        }

        public static void main(String[] args) {
            String number;// введенное число

            Scanner scanner = new Scanner(System.in);

            do {
                System.out.print("Введите целое число ПРОПИСЬЮ в диапазоне от 0 до 999, для выхода введите СТОП: ");
                number = scanner.nextLine();
                System.out.println(Third.numbersAdapter(number));

            } while (!number.equals("СТОП"));

            System.out.println("Введена команда СТОП, работа программы завершена");
        }
    }

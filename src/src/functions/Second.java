package functions;
/*
Задание 2.
Вводим число (0-999), получаем строку с прописью числа.
 */
import java.util.Scanner;

public class Second {

    private static int numberA;
    private static int numberMax = 999;
    private static String numText;// число в виде текста

    private static int units;          // единичные значение
    private static int decimal;        // десятичное значение
    private static int hundreds;       // сотни

    private static final String[][] sampleText = {{"", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"},
            {"", "десять ", "двадцать ", "тридцать ", "сорок ", "пятьдесят ", "шестьдесят ", "семьдесят ", "восемьдесят ", "девяносто "},
            {"", "сто ", "двести ", "триста ", "четыреста ", "пятьсот ", "шестьсот ", "семьсот ", "восемьсот ", "девятьсот "}};

    private static final String[] sample11to19 = {"десять ", "одинадцать ", "двенадцать ", "тринадцать ", "четырнадцать ", "пятнадцать ",
            "шестнадцать ", "семьнадцать ", "восемьнадцать ", "девятнадцать ", "девятнадцать "};



    public static String WordsRus(int number) {
        numberA = number;

        numText = "";
        if (numberA < 0 || numberA > numberMax) {
            return numText = "Число выходит за рамки указанного диапазона";
        }
        if (numberA == 0) {
            return numText = "ноль ";
        }

        // формируем текст числа прописью
        hundreds = numberA / 100;
        decimal = (numberA - (hundreds * 100)) / 10;
        units = numberA % 10;

        if (decimal == 1) numText =numText+sampleText[2][hundreds] + sample11to19[units];
        else numText = sampleText[2][hundreds] + sampleText[1][decimal] + sampleText[0][units];

        return numText;
    }

    public static void main(String[] args) {
        int number;// введенное число

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.print("Введите целое число в диапазоне 0 до 999, для выхода введите 0: ");
            number = scanner.nextInt();
            System.out.println(Second.WordsRus(number));

        } while (number != 0);

        System.out.println("Введено число 0, работа программы завершена");
    }
}




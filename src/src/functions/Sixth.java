package functions;
/*
Задание 6.
Вводим строку, которая содержит число, написанное прописью (0-999 999 999 ). Получить само число
 */
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Sixth {


    private static final String[][] sampleText = {{"ноль","один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять ",
            "одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать",
            "шестнадцать ", "семьнадцать", "восемьнадцать", "девятнадцать"},
            {"","","двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"},
            {"","сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"}};

    private static Map<String, Integer> numberAdapter = new HashMap<String, Integer>();




    public static long numbersAdapter(String number) {

        for (int i = 0; i <= 19; i++) {
            numberAdapter.put(sampleText[0][i], i);
        }
        numberAdapter.put("одна",1);
        numberAdapter.put("две",2);
        for (int i = 2; i <= 9; i++) {
            numberAdapter.put(sampleText[1][i], i * 10);
        }
        for (int i = 1; i <= 9; i++) {
            numberAdapter.put(sampleText[2][i], i * 100);
        }

        long billions = 0;
        long millions = 0;
        long thousands = 0;
        long tooThousands = 0;

        //переменная k создана для последующего разделения строки перед искомым словом ("миллиард", "миллион","тысяча" и тд)
        int k = 0;

        if (number.contains("миллиард")) {
            k = number.indexOf("миллиард");
            String o = number.substring(0,k);
            billions = toThousand(o)*1000000000;
            number = number.replaceAll("(.*)миллиардов |(.*)миллиардов|(.*)миллиарда |(.*)миллиарда|(.*)миллиард |(.*)миллиард","");
        }
        if (number.contains("миллион")) {
            k = number.indexOf("миллион");
            String o = number.substring(0, k);
            millions = toThousand(o)*1000000;
            number = number.replaceAll("(.*)миллионов |(.*)миллионов|(.*)миллиона |(.*)миллиона|(.*)миллион |(.*)миллион","");
        }
        if (number.contains("тысяч")) {
            k = number.indexOf("тысяч");
            String o = number.substring(0, k);
            thousands = toThousand(o)*1000;
            number = number.replaceAll("(.*)тысячь |(.*)тысячь|(.*)тысяча |(.*)тысяча|(.*)тысячи |(.*)тысячи","");

        }
        if (!number.isEmpty()){
           tooThousands= toThousand(number);

        }
        return billions+millions+thousands+tooThousands;
    }

    //Метод toThousand отвечает за преобразование единиц, десятков и сотен
    public static long toThousand(String u){
        long exit = 0;
        String[] a = u.split(" ");
        if (a.length == 1) {
            exit = exit + numberAdapter.get(a[0]);
        } else if (a.length == 2) {
            exit = exit + (numberAdapter.get(a[0]) + numberAdapter.get(a[1]));
        } else if (a.length == 3) {
            exit = exit + (numberAdapter.get(a[0]) + numberAdapter.get(a[1]) + numberAdapter.get(a[2]));
        }
        return exit;
    }


    public static void main(String[] args) {
        String number;// введенное число

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.print("Введите целое число ПРОПИСЬЮ в диапазоне от 0 до 999 999 999 999: ");
            number = scanner.nextLine();
            System.out.println(Sixth.numbersAdapter(number));

        } while (true);

    }
}

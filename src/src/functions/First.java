package functions;
/*
Задание 1.
Получить строковое название дня недели по номеру дня.
 */

public class First {
    public String weekAdapter(int a){
        String result = "Wrong number entered!";
        switch (a) {
            case 1:
                result = "It's Monday.";
                break;
            case 2:
                result = "It's Tuesday.";
                break;
            case 3:
                result = "It's Wednesday.";
                break;
            case 4:
                result = "It's Thursday.";
                break;
            case 5:
                result = "It's Friday.";
                break;
            case 6:
                result = "It's Saturday.";
                break;
            case 7:
                result = "It's Sunday.";
                break;
        }
        return result;
    }
}

package functions;

import org.junit.Assert;
import org.junit.Test;

public class TestSixth {
    @Test
    public void testNumbersAdapter() {
        long testValue = Sixth.numbersAdapter("девятьсот девяносто девять миллиардов девятьсот девяносто девять миллионов девятьсот девяносто девять тысячь девятьсот девяносто девять");
        Assert.assertEquals(999999999999L,testValue);
    }
}
